/*
 * Init.h
 *
 *  Created on: 13 janv. 2023
 *      Author: 47858208
 */
#ifndef MOUVEMENT_H_
#define MOUVEMENT_H_

#include <msp430.h>

void avancer();

void redemarrer();

void arreter();

void tournerDroite();

void tournerGauche();

void demiTourDroite();

void demiTourGauche();

#endif /* INIT_H_ */
