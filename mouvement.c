/*
 * Mouvements.c
 *
 */
#include "mouvement.h"
#include "capteurs.h"


void redemarrer()
{
    P2OUT&= ~BIT1;
    P2OUT|= BIT5;
    TA1CCR1 = 67;               // determine le rapport cyclique du signal TA1.1 // moteur gauche
    TA1CCR2 = 71;
}

void avancer(int *distance, int destination)
{
    P2OUT&= ~BIT1;
    P2OUT|= BIT5;

    unsigned char opt, ini=0;
    int dis = *distance;

    opt = (P2IN & BIT0); //Ici on n'utilise que l'opto pour moteur A
    while(dis <= destination){
        detecterObstaclesIR();
        detecterObstacleLum();
        opt = (P2IN & BIT0); //Ici on n'utilise que l'opto pour moteur A
        if(ini != opt){
            //Ajouter distance si un changement d'etat est detecte
            dis += 1;
            ini = opt;
        }
        __delay_cycles(200);
    }
}

void arreter()
{
    TA1CCR1 = 0;
    TA1CCR2 = 0;
}

void tournerDroite()
{
    P2OUT&= ~BIT1;
    P2OUT&= ~BIT5;
    __delay_cycles(180000); // modifier selon vitesse robot fast
}

void tournerGauche()
{
    P2OUT|=BIT1;
    P2OUT|=BIT5;
    __delay_cycles(180000); // modifier selon vitesse robot fast
}

/*void demiTourDroite()
{
    tournerDroite();
    avancer(&distance, 27); // 14.96cm
    __delay_cycles(500000);
    tournerDroite();
}

void demiTourGauche()
{
    tournerGauche();
    avancer(&distance, 27); // 14.96cm
    __delay_cycles(500000);
    tournerGauche();
}*/


