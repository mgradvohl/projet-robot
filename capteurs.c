/*
 * capteurs.c
 *
 */
#include "capteurs.h"
#include "mouvement.h"
#include "ADC.h"
#include <stdio.h>
#include <stdlib.h>
int detecterObstaclesIR(){

   ADC_Demarrer_conversion(5);

   if(ADC_Lire_resultat() >= 450){
        arreter();
    }
   else redemarrer();

   return ADC_Lire_resultat();
}

int detecterObstacleLum(){
   ADC_Demarrer_conversion(7);

   if(ADC_Lire_resultat() <= 0x02AA){
       P1OUT |= BIT6;
       tournerDroite();
       tournerDroite();
       redemarrer();
    }

   return ADC_Lire_resultat();
}
