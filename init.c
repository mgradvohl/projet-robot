/*
 * init.c
 *
 */

#include "init.h"

void initRobot()
{
    WDTCTL = WDTPW | WDTHOLD;
    BCSCTL1= CALBC1_1MHZ;       // frequence d�horloge 1MHz
    DCOCTL= CALDCO_1MHZ;        // frequence d�horloge 1MHz

    // moteur A
    P2DIR |= BIT2;              // P2.2 en sortie
    P2SEL |= BIT2;              // selection fonction TA1.1
    P2SEL2 &= ~BIT2;            // selection fonction TA1.1

    // moteur B
    P2DIR |= BIT4;              // P2.44 en sortie
    P2SEL |= BIT4;              // selection fonction TA1.2
    P2SEL2 &= ~BIT4;            // selection fonction TA1.2

    // parametres generales PWM
    P2DIR |= BIT5;              // P2.5 en sortie
    P2DIR |= BIT1;              // P2.1 en sortie
    TA1CTL = TASSEL_2 | MC_1;   // source SMCLK pour TimerA , mode comptage Up
    TA1CCTL1 |= OUTMOD_7;       // activation mode de sortie n�7 sur TA1.1
    TA1CCTL2 |= OUTMOD_7;       // activation mode de sortie n�7 sur TA1.2
    TA1CCR0 = 200;              // determine la periode du signal
    TA1CCR1 = 67;               // determine le rapport cyclique du signal TA1.1 // moteur gauche
    TA1CCR2 = 71;               // determine le rapport cyclique du signal TA1.2 // moteur droite

    // capteur IR
    // P1DIR &= ~BIT5; // entree lecture capteur
}

void initTimer()
{
    P1DIR |= BIT0;                  // bit 0 port 1 en sortie
    TA0CTL = 0|(TASSEL_2 | ID_3);   // source SMCLK, pas de predivision ID_3
    TA0CTL |= MC_3;                 // comptage en mode up-down
    TA0CTL |= TAIE;                 // autorisation interruption TAIE
    TA0CCR0 = 62500;                // 1*2*8*62500 = 1 second
    __enable_interrupt();
}

void initInterrupt()
{
    // bouton S2 (BIT3)
    P1DIR&=~(BIT3);
    P1REN|= (BIT3);                        //Activation r�sistance interne
    P1IE |= (BIT3);                   //Initialisation de l'interruption
    P1IES|= (BIT3);                   //Interruption sur front descendant
    P1IFG&=~(BIT3);                //RAZ Flag d'interruption

    __enable_interrupt();
}

void initOdometrie()
{
    P2DIR &= ~(BIT0|BIT3); //Les optos en entrees
}
