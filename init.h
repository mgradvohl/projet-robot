/*
 * init.h
 *
 */

#ifndef INIT_H_
#define INIT_H_

#include <msp430.h>

void initRobot();
void initTimer();
void initOdometrie();
void initInterrupt();

#endif /* INIT_H_ */
