#include "init.h"
#include "capteurs.h"
#include "mouvement.h"
#include "ADC.h"

/**
 * main.c
 *
 */
unsigned char sec = 0;
unsigned int distance=0;


void main(void)
{
    initRobot();
    initTimer();
    initInterrupt();
    initOdometrie();
    ADC_init();

    while(1){
        avancer(&distance, 100);
    }
}

#pragma vector=TIMER0_A1_VECTOR //voir diaporama seance precedente
__interrupt void ma_fnc_timer(void)
{
    sec++;
    if(sec>=20)
        arreter();
    TA0CTL &= ~TAIFG; //RAZ TAIFG
}

#pragma vector=PORT1_VECTOR
__interrupt void fct_arret_urgence(){
    arreter();
}
