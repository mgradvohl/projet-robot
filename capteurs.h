/*
 * capteurs.h
 *
 */

#ifndef CAPTEURS_H_
#define CAPTEURS_H_

int detecterObstaclesIR();
int detecterObstacleLum();

#endif /* capteurs_H_ */
